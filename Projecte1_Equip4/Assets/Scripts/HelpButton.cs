using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpButton : MonoBehaviour
{
    public GameObject popUp;
	public GameObject portada;
	// Start is called before the first frame update
	void Start()
	{
		this.GetComponent<Button>().onClick.AddListener(TaskOnClick);
	}
	void TaskOnClick()
	{
		if (!popUp.activeSelf)
		{
			Debug.Log("PopUp Obrint-se");
			popUp.SetActive(true);
			portada.SetActive(false);
		}
        else {
			Debug.Log("PopUp Tancant-se");
			popUp.SetActive(false);
			portada.SetActive(true);
		}
	}
}
