using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraScript : MonoBehaviour
{
    public RaycastHit2D hit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 worldPoint = this.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
            hit = Physics2D.Raycast(worldPoint, new Vector3(0,0,1f),100f);
            if (hit.collider!=null)
            {
                Debug.Log("Objecte agafat");
                hit.collider.GetComponent<SpriteScript>().CheckColor();
            }
        }
    }
}
