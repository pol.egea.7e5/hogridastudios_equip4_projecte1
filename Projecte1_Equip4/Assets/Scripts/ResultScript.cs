using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEngine; 

public class ResultScript : MonoBehaviour
{
    public TMPro.TMP_Text diagnosi;
    string jsonString;
    string fileName;
    Marcador marcador;
    // Start is called before the first frame update
    void Start()
    {   fileName = @"resultats/anteriorResultat.json";
        jsonString = File.ReadAllText(fileName);
        //ivoca readfile i json convert
        marcador = JsonConvert.DeserializeObject<Marcador>(jsonString);
        GetComponent<TMPro.TMP_Text>().text = System.Convert.ToString(marcador.puntuacio);
        Debug.Log(marcador.porcentajes[0].ToString());
        Debug.Log(marcador.porcentajes[1].ToString());
        switch (marcador.stage)
        { 
            case 1:
                diagnosi.text = "No t� daltonisme";
                break;
            default:
                if (marcador.porcentajes[0]<20 && marcador.porcentajes[1]<20)
                {
                    diagnosi.text = "T� Acromatopsia";
                }
                else if (marcador.porcentajes[0] < 80 && marcador.porcentajes[1] < 80)
                {
                    diagnosi.text = "T� Deuteranopia";
                }
                else if(marcador.porcentajes[0]< marcador.porcentajes[1])
                {
                    diagnosi.text = "T� Protan�pia";
                }
                else if(marcador.porcentajes[0] > marcador.porcentajes[1])
                {
                    diagnosi.text = "T� Tritanopia";
                }
                else
                {
                    diagnosi.text = "Pot tenir varios tipos de daltonisme";
                }
                break;
          
        }

    }
}
