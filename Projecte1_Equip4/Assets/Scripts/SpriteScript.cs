using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteScript : MonoBehaviour
{
    AudioSource[] audioSources;
    AudioSource audioCor, audioIncor;
    public SpriteMatrixScript spriteMatrix;

    // Start is called before the first frame update
    void Start()
    {
        
        if (transform.parent.GetComponent<SpriteMatrixScript>()!= null)spriteMatrix=transform.parent.GetComponent<SpriteMatrixScript>();
        if (spriteMatrix!=null)Debug.Log(spriteMatrix.lvlColors[1]);
        audioSources = GetComponents<AudioSource>();
        audioCor = audioSources[0];
        audioIncor = audioSources[1];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CheckColor()
    {
        if(GetComponent<SpriteRenderer>()!=null&&spriteMatrix!=null) 
        if (GetComponent<SpriteRenderer>().color==spriteMatrix.lvlColors[spriteMatrix.comptador*4]|| GetComponent<SpriteRenderer>().color == spriteMatrix.lvlColors[spriteMatrix.comptador * 4 + 1] || GetComponent<SpriteRenderer>().color == spriteMatrix.lvlColors[spriteMatrix.comptador * 4 + 2] || GetComponent<SpriteRenderer>().color == spriteMatrix.lvlColors[spriteMatrix.comptador * 4 + 3])
        {
            spriteMatrix.comptador += 1;
            spriteMatrix.marcador.puntuacio += 100/19;
            spriteMatrix.quadratGen = false;
            spriteMatrix.intents = 0;
            audioCor.Play();
        }
        else
        {
            spriteMatrix.marcador.puntuacio -= 5;
            spriteMatrix.intents += 1;
            audioIncor.Play();
        }
    }
}
