using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Marcador
{
    public System.DateTime data { get; set; }
    public float puntuacio { get; set; }

    public int stage { get; set; }

    public int totalPregs { get; set; }
    public float[] porcentajes { get; set; }


    public Marcador(DateTime data, float puntuacio, int stage, int totalPregs,float[] porcentajes)
    {
        this.data = data;
        this.puntuacio = puntuacio;
        this.stage = stage;
        this.totalPregs = totalPregs;
        this.porcentajes = porcentajes;
    }
    public Marcador()
    {
        this.data = DateTime.Now;
        this.puntuacio = 0;
        this.stage = 0;
        this.totalPregs = 0;
        this.porcentajes = new float[] {0,0};
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
