using Newtonsoft.Json;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Serialitzador : MonoBehaviour
{
    public Marcador marcador;
    public string jsonstring;
    public string path;
    public StreamWriter sw;
    public JsonTextWriter writer;

 
    // Start is called before the first frame update
    void Start()
    {
        marcador = new Marcador();
        marcador.data = System.DateTime.Now;
        marcador.puntuacio = PlayerPrefs.GetInt("marcador");
   
        path = "/anteriorResultat.json";

       
    }



    void TaskOnClick()
    {
        try
        {
            jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(marcador);
            File.WriteAllText(@path, jsonstring);
        }
        catch
        {
            Debug.Log("No funca");
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Button>().onClick.AddListener(TaskOnClick);
    }
}
