using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsButton : MonoBehaviour
{
    public ObjectResponsesGameScreen objecte;
    public TMPro.TMP_Text textBoto;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(TaskOnClick);
    }
    void TaskOnClick()
    {
        if (textBoto.text == objecte.actualresul)
        {
            objecte.marcador.puntuacio += 1;
           
            objecte.comptador += 1;

        }
        else if (objecte != null)
        {
            checkDaltType();
            objecte.comptador += 1;
        }
    }

    void checkDaltType()
    {
        switch (objecte.comptador)
        {
            case 11:
                if (textBoto.text == "6") objecte.countProtano = objecte.countProtano+1;
                if (textBoto.text == "2") objecte.countDeutera = objecte.countDeutera + 1;
              
                break;

            case 12:
                if (textBoto.text == "2") objecte.countProtano = objecte.countProtano + 1;
                if (textBoto.text == "4") objecte.countDeutera = objecte.countDeutera + 1;
                break;
            case 13:
                if (textBoto.text == "5") objecte.countProtano = objecte.countProtano + 1;
                if (textBoto.text == "3") objecte.countDeutera = objecte.countDeutera + 1;
                break;
            case 14:
                if (textBoto.text == "6") objecte.countProtano = objecte.countProtano + 1;
                if (textBoto.text == "9") objecte.countDeutera = objecte.countDeutera + 1;
                break;
         
        }
    }
  
}