using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Array2DEditor;

public class SpriteMatrixScript : MonoBehaviour
{
    //Array colors: 2 colors per punt, un fosc i un clar per a poder fer el canvi
    public Color[] lvlColors;
    public Color[] colorOtherSprites;
    public TMPro.TMP_Text compPregIPregTotals;
    public int comptador;
    public Marcador marcador;
    public string jsonstring;
    public string path;
    public FilaSprite[] quadrats;
    public int puntsMax;
    public int intents;
    public bool quadratGen;
    private int _xQuadrat;
    private int _yQuadrat;
    private int _comptadorFrames;
    private int _fpsPerACanviSprite;
    public int lvlint;
    public string seguentScena;
    [System.Serializable]
    public class FilaSprite
    {
        public SpriteRenderer[] fila;
    }
    void Start()
    {
        quadratGen = false;
        intents = 0;
        marcador.stage = lvlint;
        marcador.puntuacio = 0;
        comptador = 0;
        path = @"resultats/anteriorResultat.json";
        puntsMax = 19;
    
    }
    // Update is called once per frame
    void Update()
    {
        
        compPregIPregTotals.text = $"{comptador+1}/{puntsMax}";
        //Continua joc
        if (comptador<=puntsMax&& intents<=1)
        {
            _fpsPerACanviSprite = (int)Mathf.Round(Time.frameCount / Time.time * 0.3f);
            _comptadorFrames += 1;
            BorrarOCanviColorQuadrat();
            GenQuadrat();
        }
       
        if (comptador >= puntsMax)
        {
            if (marcador.stage == 2)
            {
                
                marcador.porcentajes[0] = marcador.puntuacio; 
          
            
                jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(marcador);
                System.IO.File.WriteAllText(path, jsonstring);
                SceneManager.LoadScene(seguentScena);
            }
            else
            {
                if ((marcador.stage == 3)) marcador.porcentajes[1] = marcador.puntuacio;

            
                jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(marcador);
                System.IO.File.WriteAllText(path, jsonstring);
                SceneManager.LoadScene("ResultScreen");
            }

      
        }
        if(intents == 2)
        {
            if (seguentScena != "" && seguentScena!=null)
            {
                if (marcador.stage == 2)
                {
                    marcador.porcentajes[0] = marcador.puntuacio;
                   
                    jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(marcador);
                    System.IO.File.WriteAllText(path, jsonstring);
                    SceneManager.LoadScene(seguentScena);
                }
                if (comptador >= 17)
                {

                    
                    if((marcador.stage == 3)) marcador.porcentajes[1] = marcador.puntuacio;
               
                    jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(marcador);
                    System.IO.File.WriteAllText(path, jsonstring);
                    
                  
                    SceneManager.LoadScene("ResultScreen");
                }
                else {
                    marcador.puntuacio = 0;                 
                    SceneManager.LoadScene(seguentScena);
                }
            }
            else
            {
            
                jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(marcador);
                System.IO.File.WriteAllText(path, jsonstring);
                SceneManager.LoadScene("ResultScreen");
            }
        }
    }
    //si jugador toca quadrat quadrat gen false
    void GenQuadrat()
    {
        //Gen quadrat
        if (!quadratGen) 
        {
            _xQuadrat = Random.Range(1,7);
            _yQuadrat = Random.Range(1,7);
            for(int y=_yQuadrat-1;y<=_yQuadrat+1;y++)
            { 
                for(int x = _xQuadrat - 1; x <= _xQuadrat + 1; x++)
                {
                    quadrats[y].fila[x].color = lvlColors[Random.Range(comptador * 4, comptador * 4 + 4)];
                }
            }
            quadratGen = true;
        }
    }
    void BorrarOCanviColorQuadrat()
    {
        if (!quadratGen) foreach (FilaSprite filadequadrats in quadrats) foreach (SpriteRenderer objecte in filadequadrats.fila) objecte.color = colorOtherSprites[Random.Range(0,1)];
        else
        {
            if(_comptadorFrames>=_fpsPerACanviSprite)foreach (FilaSprite filadequadrats in quadrats) foreach (SpriteRenderer objecte in filadequadrats.fila)
                {
                    if (objecte.color == lvlColors[comptador * 4] || objecte.color == lvlColors[comptador * 4 + 1] || objecte.color == lvlColors[comptador * 4 + 2] || objecte.color == lvlColors[comptador * 4 + 3]) objecte.color = lvlColors[Random.Range(comptador * 4, comptador * 4 + 4)];
                    else objecte.color = colorOtherSprites[Random.Range(0, 4)];
                        _comptadorFrames = 0;
                    
                }
        }
    }


}





