
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.SceneManagement;

public class ReturnMenu : MonoBehaviour
{
   
 
    // Start is called before the first frame update
    void Start()
	{
		this.GetComponent<Button>().onClick.AddListener(TaskOnClick);
	}
	void TaskOnClick()
	{
               
        
            SceneManager.LoadScene("MenuScreen");
      
	}
}
