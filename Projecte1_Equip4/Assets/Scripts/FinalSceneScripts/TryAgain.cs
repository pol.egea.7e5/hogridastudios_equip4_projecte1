using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using Newtonsoft.Json;

public class TryAgain : MonoBehaviour
{
    public Marcador marcador;
    public string jsonstring;
    public string path;
    public StreamWriter sw;
    public JsonTextWriter writer;
    public void Awake()
    {
        marcador = new Marcador();
        marcador.data = System.DateTime.Now;
        marcador.puntuacio = PlayerPrefs.GetInt("marcador");
        path = "/anteriorResultat.json";
    }
    // Start is called before the first frame update
    void Start()
	{
		this.GetComponent<Button>().onClick.AddListener(TaskOnClick);
	}
	void TaskOnClick()
	{
        try
        {
            jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(marcador);
            File.WriteAllText(@path, jsonstring);
        }
        catch
        {
            Debug.Log("No funca");
        }
        SceneManager.LoadScene("TESTGAME");
	}
}
