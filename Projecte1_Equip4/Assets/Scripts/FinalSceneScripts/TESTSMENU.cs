using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TESTSMENU : MonoBehaviour
{
    public TMPro.TMP_Text enunciat;
    public int marcador;
    public int actualresul;
    public int comptador;
    public TMPro.TMP_Text[] llistaOpcions;
    public SpriteRenderer imatge;
    public int comptaopcions = 0;
    public TMPro.TMP_Text compPregIPregTotals;
    [System.Serializable]
    public class QandA
    {
        public bool enunciat;
        public Sprite img;
        public int[] respostes;
        public int correcta;
        public QandA(bool enunciat, Sprite img, int[] respostes, int correcta)
        {
            this.enunciat = enunciat;
            this.img = img;
            this.respostes = respostes;
            this.correcta = correcta;
        }
    }
    public QandA[] totalPreguntes= { };
    // Start is called before the first frame update
    void Start()
    {
        marcador = 0;
        comptador = 0;
    }
    // Update is called once per frame
    void Update()
    {
        if (comptador >= totalPreguntes.Length) {
            PlayerPrefs.SetInt("marcador",marcador);
            SceneManager.LoadScene("TESTRESULT"); 
        }
        else
        {
            imatge.sprite = totalPreguntes[comptador].img;
            actualresul = totalPreguntes[comptador].correcta;
            foreach (TMPro.TMP_Text opcio in llistaOpcions)
            {
                opcio.text = System.Convert.ToString(totalPreguntes[comptador].respostes[comptaopcions]);
                comptaopcions++;
            }
            comptaopcions = 0;
            Enunciat();
        }
        compPregIPregTotals.text = $"{comptador}/{totalPreguntes.Length}";
    }
    void Enunciat()
    {
        if (totalPreguntes[comptador].enunciat) enunciat.text = "Quin n�mero veus?";
        else enunciat.text = "Quants camins veus?";
    }
}
