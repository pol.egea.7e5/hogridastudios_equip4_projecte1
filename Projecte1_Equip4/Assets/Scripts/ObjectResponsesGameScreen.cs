using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ObjectResponsesGameScreen : MonoBehaviour
{
    public TMPro.TMP_Text[] llistaOpcions;
    public TMPro.TMP_Text compPregIPregTotals;
    public TMPro.TMP_Text enunciat;
    public SpriteRenderer imatge;

    public string actualresul;
    public int comptador;
    public int comptaopcions = 0;
   
    public int countProtano;
    public int countDeutera;


    [System.Serializable]
    public class QandA
    {
        public bool enunciat;
        public Sprite img;
        public string[] respostes;
        public string correcta;
        
      
        public QandA(bool enunciat, Sprite img, string[] respostes, string correcta)
        {
            this.enunciat = enunciat;
            this.img = img;
            this.respostes = respostes;
            this.correcta = correcta;
        }
    }




    



    public QandA[] totalPreguntes= { };
    public Marcador marcador;
    public string jsonstring;
    public string path;
    void Start()
    {
        countDeutera=0;
        countProtano=0;
        
        marcador.stage = 0;
        marcador.puntuacio = 0;
        comptador = 0;
        path = @"resultats/anteriorResultat.json";
        countDeutera = 0;
        countProtano = 0;
    
    }




    // Update is called once per frame
    void Update()
    {
        if ((comptador >= totalPreguntes.Length)||(comptador==11&&marcador.puntuacio==11)) {

            if(comptador>=11)   Daltonic();
            else marcador.stage = 0;
            jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(marcador);
            System.IO.File.WriteAllText(path, jsonstring);
            SceneManager.LoadScene("ResultScreen"); 
           
        }
        else
        {
            imatge.sprite = totalPreguntes[comptador].img;
            actualresul = totalPreguntes[comptador].correcta;
            foreach (TMPro.TMP_Text opcio in llistaOpcions)
            {
                opcio.text = totalPreguntes[comptador].respostes[comptaopcions];
                comptaopcions++;
            }
            comptaopcions = 0;
            Enunciat();
           

        }
        compPregIPregTotals.text = $"{comptador}/{totalPreguntes.Length}";
    }
    
    
    
    void Enunciat()
    {
        if (totalPreguntes[comptador].enunciat) enunciat.text = "Quin número veus?";
        else enunciat.text = "Quants camins veus?";
    }


    public void difNums()
    {
        switch (comptador)
        {
            case 11:

                break;
        }

    }
    void Daltonic()
    {
        if (marcador.puntuacio==0)            marcador.stage = 5;
        else
        {
            if (countDeutera < countProtano) marcador.stage = 1;
            if (countDeutera < countProtano && countDeutera < 3) marcador.stage = 2;
            if (countDeutera > countProtano) marcador.stage = 3;
            if (countDeutera > countProtano && countProtano < 3) marcador.stage = 4;

        
        }
    }
      
       
    }




